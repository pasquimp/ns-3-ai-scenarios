# ns-3-ai scenarios

You should clone and build ns-3 with the ai module in advance following the instructions:

	$ git clone gitblab.com/nsnam/ns-3-dev.git ns-3.38-ai
	$ cd ns-3.38-ai
	$ git checkout -b ns-3.38

	$ cd contrib
	$ git clone -b cmake https://github.com/hust-diangroup/ns3-ai.git

	$ cd ..
	$ ./ns3 configure
	$ ./ns3 build

	$ cd contrib/ns3-ai/py_interface
	$ pip3 install . --user

Then, you should clone this repo:

	$ cd scratch
	$ git clone https://gitlab.com/pasquimp/ns-3-ai-scenarios.git

Finally, you should move in ns-3-ai-scenarios folder and start with first-ai example:

	$ cd ns-3-ai-scenarios
	$ python3 run-first-ai-example.py

