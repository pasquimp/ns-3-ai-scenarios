# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-
# Copyright (c) 2020 Huazhong University of Science and Technology, Dian Group
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author: Pengyu Liu <eic_lpy@hust.edu.cn>
#         Hao Yin <haoyin@uw.edu>

# An example for the ns3-ai model to illustrate the data exchange
# between python-based AI frameworks and ns-3.
#
# In this example, we have two variable a and b in ns-3,
# and then put them into the shared memory using python to calculate
#
#       c = a + b
#
# Finally, we put back c to the ns-3.

import random
from ctypes import *
import numpy as np
from tensorforce import Agent

from py_interface import *

MAX_NUM_LINKS = 100

# The environment (in this example, contain 'a' and 'b')
# shared between ns-3 and python with the same shared memory
# using the ns3-ai model.
class Env(Structure):
    _pack_ = 1
    _fields_ = [
        ('a', c_int),
        ('b', c_int),

        ('queueStatus', c_int * MAX_NUM_LINKS),
        ('reward', c_int),
    ]

# The result (in this example, contain 'c') calculated by python
# and put back to ns-3 with the shared memory.
class Act(Structure):
    _pack_ = 1
    _fields_ = [
        ('c', c_int),

        ('linkAction', c_double * MAX_NUM_LINKS),

    ]


#ns3Settings = {'a': 20, 'b': 30}
mempool_key = 1234                                          # memory pool key, arbitrary integer large than 1000
mem_size = 4096                                             # memory pool size in bytes
memblock_key = 2335                                         # memory block key, need to keep the same in the ns-3 script
using_waf = False

Init(1234, 4096)

rl = Ns3AIRL(memblock_key, Env, Act)                    # Link the shared memory block with ns-3 script


exp = Experiment(mempool_key, mem_size, 'first-ai', '../../', using_waf)      # Set up the ns-3 environment
exp.reset ()
pro = exp.run(show_output=True)    # Set and run the ns-3 script (sim.cc)

# Instantiate a Tensorforce agent
agent = Agent.create(
            agent='tensorforce',
            states=dict(type='float', shape=(MAX_NUM_LINKS,)),
            actions=dict(type='int', shape=(MAX_NUM_LINKS,), num_values=3),
            #environment=environment  # alternatively: states, actions, (max_episode_timesteps)
            memory=10000,
            update=dict(unit='timesteps', batch_size=64),
            optimizer=dict(type='adam', learning_rate=3e-4),
            policy=dict(network='auto'),
            objective='policy_gradient',
            reward_estimation=dict(horizon=20)
        )

try:
    #for i in range(1):
    #    exp.reset()                                             # Reset the environment
        #rl = Ns3AIRL(memblock_key, Env, Act)                    # Link the shared memory block with ns-3 script
        #ns3Settings['a'] = random.randint(0,10)
        #ns3Settings['b'] = random.randint(0,10)
        #pro = exp.run(show_output=True)    # Set and run the ns-3 script (sim.cc)
    while not rl.isFinish():
            print ("not finished")
            with rl as data:
                print ("checking data")
                if not data:
                    break
                print ("[Agent]: reading queue Status")
                for i in data.env.queueStatus :
                     print(data.env.queueStatus [i], end = ' ')
                print()
                print ("[Agent]: reading reward")
                print (data.env.reward)

                # AI algorithms here and put the data back to the action
                states = np.asarray (data.env.queueStatus)
                print (states)
                actions = agent.act(states=states, independent=False)
                agent.observe(terminal=False, reward=data.env.reward)

                print ("printing actions")
                print  (actions)
                for i in range(MAX_NUM_LINKS):
                    data.act.linkAction [i] = actions [i]
                print ("action computed")

    pro.wait()                                              # Wait the ns-3 to stop
    print ("ns-3 finished")
except Exception as e:
    print('Something wrong')
    print(e)
finally:
    del exp
