/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// Default Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstScriptExample");

//#include "ns3/core-module.h"
#include "ns3/ns3-ai-module.h"
#include "ns3/log.h"

using namespace std;
//using namespace ns3;

//NS_LOG_COMPONENT_DEFINE("a_plus_b");

/**
 * \brief Shared memory to store a and b.
 *
 * This struct is the environment (in this example, contain 'a' and 'b')
 * shared between ns-3 and python with the same shared memory
 * using the ns3-ai model.
 */


#define MAX_NUM_LINKS 100


struct Env
{
    int a;
    int b;
    uint32_t queueStatus[MAX_NUM_LINKS];
    int reward;

}Packed;

/**
 * \brief Shared memory to store action c.
 *
 * This struct is the result (in this example, contain 'c')
 * calculated by python and put back to ns-3 with the shared memory.
 */
struct Act
{
    int c;
    double linkActions[MAX_NUM_LINKS];

}Packed;

/**
 * \brief A class to calculate APB (a plus b).
 *
 * This class shared memory with python by the same id,
 * and got two variable a and b, and then put them into the shared memory
 * using python to calculate c=a+b, and got c from python.
 */
class APB : public Ns3AIRL<Env, Act>
{
public:
    APB(uint16_t id);
    int Func(Ptr<NetDevice> dev, int a, int b);
};

/**
 * \brief Link the shared memory with the id and set the operation lock
 *
 * \param[in] id  shared memory id, should be the same in python and ns-3
 */
APB::APB(uint16_t id) : Ns3AIRL<Env, Act>(id) {
    SetCond(2, 0);      ///< Set the operation lock (even for ns-3 and odd for python).
}

/**
 * \param[in] a  a number to be added.
 *
 * \param[in] b  another number to be added.
 *
 * \returns the result of a+b.
 *
 * put a and b into the shared memory;
 * wait for the python to calculate the result c = a + b;
 * get the result c from shared memory;
 */
int APB::Func(Ptr<NetDevice> dev, int a, int b)
{
    Ptr<PointToPointNetDevice> p2pDev = DynamicCast<PointToPointNetDevice> (dev);
    int bytes = p2pDev->GetQueue ()->GetNBytes ();

    auto env = EnvSetterCond();     ///< Acquire the Env memory for writing
    env->a = bytes;
    env->b = b;

    for (int i = 0; i < MAX_NUM_LINKS; i++)
      {
        env->queueStatus[i] = 10; // random values
      }
    env->reward = 1; // random reward

    std::cout << "Env with a = " << bytes << " and b = " << b << std::endl;
    SetCompleted();                 ///< Release the memory and update conters
    NS_LOG_DEBUG ("Ver:" << (int)SharedMemoryPool::Get()->GetMemoryVersion(m_id));
    std::cout << "Waiting in 1" << std::endl;
    auto act = ActionGetterCond();  ///< Acquire the Act memory for reading
    int ret = act->c;

    std::cout << "[ns-3]: reading actions provided by the agent" << std::endl;
    for (int i = 0; i < MAX_NUM_LINKS; i++)
      {
        double action = act->linkActions[i];
        std::cout << action << " ";
      }
      std::cout << std::endl;

    GetCompleted(); ///< Release the memory, roll back memory version and update conters
    std::cout << "Waiting in 2" << std::endl;

    NS_LOG_DEBUG ("Ver:" << (int)SharedMemoryPool::Get()->GetMemoryVersion(m_id));

    Simulator::Schedule(Seconds(3), &APB::Func, this, dev, a, b);

    return ret;
}

/*
int main(int argc, char *argv[])
{
    int memblock_key = 2333;        ///< memory block key, need to keep the same in the python script
    int a = 1;
    int b = 2;
    CommandLine cmd;
    cmd.AddValue ("a","the value of a",a);
    cmd.AddValue ("b","the value of b",b);
    cmd.AddValue ("key","memory block key",memblock_key);
    cmd.Parse (argc, argv);
    APB apb(memblock_key);
    std::cout << a << "+" << b << "=" << apb.Func(a, b) << std::endl;
    apb.SetFinish();
    return 0;
}
*/

int
main(int argc, char* argv[])
{
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);
    LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);

    NodeContainer nodes;
    nodes.Create(2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("1Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    NetDeviceContainer devices;
    devices = pointToPoint.Install(nodes);

    InternetStackHelper stack;
    stack.Install(nodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");

    Ipv4InterfaceContainer interfaces = address.Assign(devices);

    UdpEchoServerHelper echoServer(9);

    ApplicationContainer serverApps = echoServer.Install(nodes.Get(1));
    serverApps.Start(Seconds(1.0));
    serverApps.Stop(Seconds(10.0));

    UdpEchoClientHelper echoClient(interfaces.GetAddress(1), 9);
    echoClient.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient.SetAttribute("Interval", TimeValue(Seconds(0.001)));
    echoClient.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps = echoClient.Install(nodes.Get(0));
    clientApps.Start(Seconds(2.0));
    clientApps.Stop(Seconds(10.0));

    int memblock_key = 2335;        ///< memory block key, need to keep the same in the python script
    int a = 1;
    int b = 2;
    //CommandLine cmd;
    //cmd.AddValue ("a","the value of a",a);
    //cmd.AddValue ("b","the value of b",b);
    //cmd.AddValue ("key","memory block key",memblock_key);
    //cmd.Parse (argc, argv);
    APB apb(memblock_key);

    //apb.Func(a, b);
    //Simulator::Schedule(Seconds(1), &FuncWrapper, apb, a, b);

    std::cout << a << "+" << b << "=" << apb.Func(devices.Get(0), a, b) << std::endl;

    Simulator::Stop (Seconds (10));
    Simulator::Run();
    Simulator::Destroy();

    apb.SetFinish();
    //return 0;

    return 0;
}
